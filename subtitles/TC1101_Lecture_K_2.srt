1
00:00:00,080 --> 00:00:07,352
Welcome to this OST2 lecture about how to enable 
parameter encryption when using the wolfTPM TSS library.

2
00:00:07,352 --> 00:00:12,680
So how this looks when using the wolfTPM
stack? First we need to create a primary key.  

3
00:00:12,680 --> 00:00:18,960
Because this primary key will be used to create 
the shared secret required for the TPM session in  

4
00:00:18,960 --> 00:00:24,840
order to enable parameter encryption. And we will talk 
about this more in a moment. Then we need to make  

5
00:00:24,840 --> 00:00:31,280
sure that our session is an HMAC type and we have 
set the proper attributes. We just mentioned them, but  

6
00:00:31,280 --> 00:00:36,520
I'll repeat them because they're important. We 
have continueSession, decrypt, and encrypt.  

7
00:00:36,520 --> 00:00:42,280
Once this is done we can execute the next TPM command 
and have the protection against machine-in-the-middle 

8
00:00:42,280 --> 00:00:48,320
attacks enabled. Once all of our operations 
with the TPM are done, the same way we would flush  

9
00:00:48,320 --> 00:00:54,320
and unload the TPM loaded objects because as you 
remember we have a limited key slots, we will also  

10
00:00:54,320 --> 00:01:00,680
flush the TPM session. How this workflow looks 
with an API? Same as in our previous lecture we  

11
00:01:00,680 --> 00:01:08,240
using the rich API wolfTPM called wrappers. We already 
learned about the wolfTPM2_CreatePrimaryKey  

12
00:01:08,240 --> 00:01:15,370
wrapper API in lecture J. Today we'll learn about 
wolfTPM2_StartSession and wolfTPM2_SetAuthSession.

13
00:01:15,370 --> 00:01:20,680
The first one creates the new TPM session, 
and the second helps us set the proper attributes  

14
00:01:20,680 --> 00:01:26,400
to enable parameter encryption. The last API that 
you probably already familiar with is the  

15
00:01:26,400 --> 00:01:33,400
wolfTPM2_UnloadHandle that just takes an index, 
a valid TPM index, and flushes the TPM object.  

16
00:01:33,400 --> 00:01:38,920
On the left side here we have the prototype of 
the wolfTPM2_StartSession. And on the right  

17
00:01:38,920 --> 00:01:44,520
side I added extra information about why we use 
a primary key when creating the HMAC session for  

18
00:01:44,520 --> 00:01:52,480
parameter encryption. There are two main categories: 
salted session and bound session. And depending on  

19
00:01:52,480 --> 00:01:57,960
that we have the four variations listed here 
on the slide. By default the tpm2-tools we used  

20
00:01:57,960 --> 00:02:04,600
in lecture G create salted and bound session if we 
provide a key context when starting a new TPM  

21
00:02:04,600 --> 00:02:11,840
session. When using API the choice is ours. Shall 
we create salted and bound session? Or shall it be  

22
00:02:11,840 --> 00:02:17,680
just salted session. The creation of salted and bound 
session is responsibility for the stack. There is  

23
00:02:17,680 --> 00:02:23,360
a lot of computation going behind the scenes into 
creating the session key, using the key derivation  

24
00:02:23,360 --> 00:02:27,840
functions and so on. It is not a responsibility 
of the application developer to know these  

25
00:02:27,840 --> 00:02:34,400
inner workings. Here it is important to know that 
depending on how we provide entropy to the TSS to  

26
00:02:34,400 --> 00:02:40,120
generate the session key this defines the strength 
of our parameter encryption. Highest strength comes  

27
00:02:40,120 --> 00:02:47,520
from a salted and bound session. At the same time 
salted session is my preference, because it already  

28
00:02:47,520 --> 00:02:54,680
provides high guarantees coming from the entropy 
from an object already created inside the TPM.  

29
00:02:54,680 --> 00:03:00,760
We use the loaded primary asymmetric key to feed 
entropy to generate the session key. This is very  

30
00:03:00,760 --> 00:03:06,080
secure and allows mitigation against machine-in-the-middle
attacks. In the wolfTPM2_StartSession

31
00:03:06,080 --> 00:03:11,240
prototype there is an interesting parameter 
at the end: encrypt decrypt algorithm (encDecAlg). 

32
00:03:11,240 --> 00:03:17,200
This parameter specifies the algorithm for parameter 
encryption. This is a topic we did not cover  

33
00:03:17,200 --> 00:03:23,160
previously because again tpm2-tools make it really 
straightforward to enable parameter encryption.

34
00:03:23,160 --> 00:03:29,200
Now we're going to be developers using the TSS API 
we need to know the two variations either  

35
00:03:29,200 --> 00:03:36,800
AES-CFB or XOR. Notice that if we don't provide either, if 
we use NULL in this parameter, parameter encryption  

36
00:03:36,800 --> 00:03:42,160
cannot be enabled for this TPM session. Let's 
take a closer look at how we typically use this  

37
00:03:42,160 --> 00:03:49,040
wrapper API. First we need to provide the current 
wolfTPM device context. This is something we usually  

38
00:03:49,040 --> 00:03:55,200
create at the start of our program when we call 
the wolfTPM2_Init API that we covered in the  

39
00:03:55,200 --> 00:04:02,280
previous lecture, lecture J and its labs. Then we 
need naturally to provide an empty variable of type  

40
00:04:02,280 --> 00:04:11,080
WOLFTPM2_SESSION to store the newly created TPM 
session. Then in place of the TPM key parameter we  

41
00:04:11,080 --> 00:04:18,000
specify a primary object that is loaded into the 
TPM to add salt to the creation of this session.

42
00:04:18,000 --> 00:04:26,680
In this case we chose a salted session therefore we 
specify NULL in place of the bind parameter. In case  

43
00:04:26,680 --> 00:04:34,720
we want salted and bound session then we can use the 
index handle for the loaded primary object here.

44
00:04:34,720 --> 00:04:41,320
I have just chosen for our parameter encryption use 
case to use salted session, salted unbound session.  

45
00:04:41,320 --> 00:04:48,520
Last but not least we need to specify a parameter 
for parameter encryption. And this can be AES-CFB or  

46
00:04:48,520 --> 00:04:54,880
XOR. It is important to know that using AES-CFB 
requires more computation on the stack but it  

47
00:04:54,880 --> 00:05:00,840
offers much stronger encryption compared to XOR. 
Now there's an important point here about  

48
00:05:00,840 --> 00:05:07,960
index. Index specifies a slot in the authorization 
area of a TPM command. And we have not talked a lot  

49
00:05:07,960 --> 00:05:12,760
about authorization areas because this requires 
us to dig deeper into how TPM commands are  

50
00:05:12,760 --> 00:05:19,040
constructed, the architecture of TPM commands, 
which can get quite complex. I'll talk about it  

51
00:05:19,040 --> 00:05:25,640
to give you the idea and understanding of what an 
authorization area is just remember that usually  

52
00:05:25,640 --> 00:05:33,760
TPM sessions are placed at the very beginning at 
index zero. And then later the wolfTPM stack handles that  

53
00:05:33,760 --> 00:05:38,920
when we add authorization, password authorization, 
or something else it knows recognizes there's  

54
00:05:38,920 --> 00:05:43,840
already a session at index zero, I'll place 
everything else afterwards. Would that be session  

55
00:05:43,840 --> 00:05:50,120
one, would that be slot one or slot two. The TPM 
commands have mandatory fields. Like for example  

56
00:05:50,120 --> 00:05:56,960
the TPM tag, the command size and the command code. 
The command code is just a numerical expression of  

57
00:05:56,960 --> 00:06:03,680
the TPM command so the TPM knows which command to 
execute. For us they come in a human readable form,  

58
00:06:03,680 --> 00:06:10,000
if it's an API or a tpm2-tool. But for the TPM it's 
like an op-code. It's just a numeric expression of  

59
00:06:10,000 --> 00:06:15,960
the TPM command. So where comes the authorization 
area? Well the authorization area comes right after  

60
00:06:15,960 --> 00:06:22,880
but it's optional. It depends on the TPM tag. As you 
can see it's already getting complicated. And you  

61
00:06:22,880 --> 00:06:28,960
don't need to know all of this when you're using 
a TSS API. This is why I said this is not really  

62
00:06:28,960 --> 00:06:34,400
an information for the application developer. This 
is usually something that software stack developer  

63
00:06:34,400 --> 00:06:41,840
would know or would really get deep into. So this 
authorization area has up to three slots. This is  

64
00:06:41,840 --> 00:06:46,640
the place where we provide policy authorization, 
password authorization, or the sessions for  

65
00:06:46,640 --> 00:06:52,560
auditing and parameter encryption. How do we use 
these slots it's up to us. Of course we need to  

66
00:06:52,560 --> 00:06:58,600
satisfy the TPM requirements. For example, if we 
have created a key with password authorization,  

67
00:06:58,600 --> 00:07:04,880
we need to provide at a slot this password 
authorization. Then if we want to protect against  

68
00:07:04,880 --> 00:07:11,480
Machine-in-the-Middle attacks, okay we need to use 
the second slot to set HMAC session with parameter  

69
00:07:11,480 --> 00:07:16,600
encryption attributes set and so on and so on. 
The good thing is that wolfTPM takes care of the  

70
00:07:16,600 --> 00:07:23,680
management of this. At least most of it. As you saw 
(wolfTPM2_)SetAuthSession allows us to say where to place  

71
00:07:23,680 --> 00:07:29,960
this session. And usually we choose the first slot 
or slot zero. Because we count from zero to two in  

72
00:07:29,960 --> 00:07:36,280
this case. When we set the authorization using the 
other wrapper APIs wolfTPM will automatically detect  

73
00:07:36,280 --> 00:07:41,200
okay there's already a session there's already 
information in slot zero so I'll use the next  

74
00:07:41,200 --> 00:07:46,760
available slot slot one. And if there's information 
in slot one it will use the information in slot two.  

75
00:07:46,760 --> 00:07:52,320
There's a lot of wolfTPM2_SetAuth* wrappers that 
you can check in the documentation of the stack,  

76
00:07:52,320 --> 00:08:00,040
to get a better sense of how wolfTPM manages the 
different auth slots and options. To perform the labs  

77
00:08:00,040 --> 00:08:06,120
associated with this lecture I would recommend 
using the outcome of the previous lab. Because we  

78
00:08:06,120 --> 00:08:14,200
already entered into using the wolfTPM API, and you 
be just upgrading and modifying that to fulfill  

79
00:08:14,200 --> 00:08:20,640
the exercises for this lecture. Of course if 
you have difficulty you can always check the  

80
00:08:20,640 --> 00:08:26,600
available solutions at our code repo in GitLab. 
If you have questions please feel free to reach  

81
00:08:26,600 --> 00:08:32,240
out in the discussion boards after each unit. This 
helps us understand the context of your question.  

82
00:08:32,240 --> 00:08:37,520
And of course you're welcome to write us an email 
at the course email written on slide. Thank you

